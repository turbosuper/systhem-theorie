clear
home
close all

%Logaritmischgetelte  Achse erzeugen
w = logspace (-3, 2, 500);

%Variabel aus der Aufgebnstellung
T = 10; %tau - zeitkonstante fuer R/C Berechnung
R = 1e3;
C = T/R;

%G2  System mithilfer Dummy Funktion darstellen
s = tf('s');
G = 1/((1+s*T)^2);

%Matrix, Spalten-, und Zeilevektor darstellen
[A, b, c, d] = linmod('A22simulink');

%Ubertragungsfunktion bauen - erst als Zustandmodell
Gstern = ss(A, b, c, d);

bode(G, Gstern, w)
grid