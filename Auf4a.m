clear
home
close all

ta = 0;
te = 10;
dt = 0.001;
t_ = ta : dt : te; %Zeit Vektor bestimmen
T = 2; % obere Grenze geben

s_ = sigma_(t_) .* exp(-t_/T);
plot(t_,s_)

s_q = s_ .^2;
e1_ =  sum(s_q) * dt %Integral