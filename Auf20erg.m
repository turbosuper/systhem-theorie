clear
home
close all

%Logaritmischgetelte  Achse erzeugen
w = logspace (-1, 3, 1000);

%G2  System mithilfer Dummy Funktion darstellen
s = tf('s');
uetf2 = (4*s^2+13*s+20)/((2*s^2+8)*(s+1));

% Zaehler und Nenner der G2 ermitteln
[z2, n2] = tfdata(uetf2) ;

%...da z und n kommen aus Arrays, wir nehmen benutzten der erster Element
z2 = z2{1,1};
n2 = n2{1,1};

%in V-Normalform darstellen
TF2VN(z2,n2);
s = tf('s');

G2_a = tf(2.5,1); %Ubertragensfunktion von V fuer bode()
G2_b = 1; %Diese Funktionen kann man vernachlaessigen
%Zaehlerdynamik sind Vorhaltersglieder
%Nennerdynamik sind Verzogerungrsglieder

G2_c =(1 + 2*0.72672*0.44721*s + 0.2*s^2); %Vorhalteglied 2. Ordnung - AUS
                                            % DER FOLIE 17 ABLESEN
G2_d = 1/(1 + 2*-5.5511e-17*0.5*s + 0.25*s^2);%Verzoegerungsglied 2. Ordnung 
                                               %  FOLIE 20 

G2_e = 1/(1 + 1*s); %Verzögerungsglied 1. Ordnung FOLIE 15

subplot(3,2,1)
bode(G2_a, w)
title('Darstellung der Verstaerkung')
grid on

subplot(3,2,3)
bode(G2_c, w)
title('Vorhalteglied 2. Ordnung')
grid on

subplot(3,2,5)
bode(G2_d, w)
title('Verzoegerungsglied 2. Ordnung')
grid on

subplot(3,2,2)
bode(G2_e, w)
title('Verzögerungsglied 1. Ordnung')
grid on

subplot(3,2,4)
bode(G2_a, 'b', G2_c, 'r', G2_d, 'y', G2_e, 'g', w)
title('Darstellung der Ganzen summe')
grid on

subplot(3,2,6)
bode(uetf2)
title('Darstellung der LTI')
grid on