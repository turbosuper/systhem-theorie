clear
home
close all

te = 10;
dt = 0.001;
t_ = -te: dt :te;
T = 2; %Zeitkonstate
a = 5; %Konstatne fuer Sigma Funktion
T0 = 4; %Dauer der Eingangs Impuls

u = a * ( sigma_(t_) - sigma_(t_ - T0) ); % Eingang signal 
g0 = (1/T) .* sigma_(t_) .* exp(-t_ / T); % Sistem Funktion g(t)

t1_ = -2*te : dt : 2*te;  %die conv Funktion erzeugt doppelte lange,
                          %dazu diese Umformung
                            
y = dt * conv(u, g0); %Ausgang: 
                      %Ubertragungs Funktion mit Hilfe der Faltungsintegral

gT1 = (1/T) .* sigma_(T0 - t_) .* exp( -(T0-t_) / T); % Sistem Funktion g(t)
                                                      % gespiegelt und
                                                      % verschoben

subplot(4,1,1);
plot(t_, u, 'linewidth', 2); %Signal Funktion
title('Urschprunglicher Eingang Signal');

subplot(4,1,2);
plot(t_, g0, 'linewidth', 2); %Sistem Funktion
title('Sistem Funktion');

subplot(4,1,3);
plot(t_, gT1, 'linewidth', 2); 
title('gespiegelte und verschobene sistem Funktion');

subplot(4,1,4);
plot(t1_, y, 'linewidth', 2); %Ubertragungs Funktion
title('Asugang Funktion');