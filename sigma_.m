function[y_] = sigma_(u_)

n = length(u_);

for i = 1:n ;
    if u_(i) >= 0
        y_(i) = 1;
    else
        y_(i) = 0;
    end
end

