clear
home
close all

% Zaehler und Nenner deklarieren
z1 = [1 4];  
n1 = [ 1 3];

z2 = [0 1 5 4];  
n2 = [1 4 8 0];


z3 = [0 2 3];  
n3 = [1 2 -3];

% Ubertragunsfunktion der Polynom 
uetf1 = tf(z1, n1);
uetf2 = tf(z2, n2);
uetf3 = tf(z3, n3);

% Sprungantwort und Poll/Nullstelen anzeigen
figure(1)
step(uetf1)
title('Sprung Antwort der G1')

figure(2)
pzmap(uetf1)
title('Poll/Nullstellen der G1')
pause


figure(1)
step(uetf2)
title('Sprung Antwort der G2')

figure(2)
pzmap(uetf2)
title('Poll/Nullstellen der G2')
pause

figure(1)
step(uetf3)
title('Sprungantwort der G3')

figure(2)
pzmap(uetf3)
title('Poll/Nullstellen der G3')
