clear
home
close all

te = 10;
dt = 0.001;
t_ = -te: dt :te;

s1 = sigma_(t_ - 1) - sigma_(t_ - 3);
s2 = sigma_(t_ - 2) - 2* sigma_(t_ - 3) + sigma_(t_ - 4);

t1_ = -2*te :dt : 2*te;  %die conv Funktion erzeugt doppelte lange,
                         %dazu diese Umformung
s1_flip = fliplr(s1);
phi0 = dt * conv(s1_flip, s2);

subplot(3,1,1);
plot(t_, s1, 'linewidth', 2); %Erste Funktion

subplot(3,1,2);
plot(t_, s2, 'linewidth', 2); %Zweite Funktion

subplot(3,1,3);
plot(t1_, phi0, 'linewidth', 2); %Korrelationfunktion
