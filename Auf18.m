clear
home
close all

% Dummy LTI Funktion erstellen
s = tf('s')

% System mithilfer dieser Dummy Funktion darstellen
uetf = (3*s+12)/((s^2+2*s+4)*(s+6))

figure(1)
pzmap(uetf)
title('Poll/Nullstellen zeigen')
pause
%BEMERKUNG - Pollpaar ist dominant, da pollpaar weit nach rechts liegt
%           Die Pollstelle an s = -6 wird bei Nullstele an -4 erschwacht

%H(s) erzeugen - die Laplace transformierte dieser G
H = uetf/s

% Zahler und Nenner erzeugen
[z,n] = tfdata(H);

%...da z und n kommen aus Arrays, wir nehmen benutzten der erster Element
z = z{1,1};
n = n{1,1};

%Partialbruchzerlegung
[r, p, k] = residue(z, n) 

%Der Pollstele be -6 raus (das ist der erster Elment dieser Vektor
%H1 = funktion ohne diese Pollstelle. Einfach Subtrahieren
H1 = H - (r(1))/(s-p(1));

figure(1)
impulse(H)
title('Stossantwort der Funktion mit alle Pollstellen')

figure(2)
impulse(H1)
title('Stossantwort der Funktion ohne Pollstelle bei -6')

figure(3)
impulse(H,H1)
title('Vergleich der Stossantwort der H und H1')
