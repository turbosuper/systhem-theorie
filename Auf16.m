clear
home
close all

% Zaehler und Nenner deklarieren
z1 = [ 0 0 1 2];  
n1 = [1 4 3 0];
 
% Polynom Darstellen
 uetf = tf(z1, n1)
 
% Produktform bauen
 [G1Null G1Poll G1K ]  = tf2zp(z1, n1) 
 
 
% Partialbruchzerlegung bauen
 [G1r G1p G1k ]  = residue(z1, n1) 

% V- Normalformdarstellen 
TF2VN(z1,n1)

pause; 

% G2
z2 = [ 0 4 20 24];  
n2 = [2 8 16 0];
uetf = tf(z2, n2)
[G2Null G2Poll G2K ]  = tf2zp(z2, n2)
[G2r G2p G2k ]  = residue(z2, n2) 
TF2VN(z2,n2)

pause;

% G3
z3 = [0 0 0 1];  
n3 = [1 1 0 0];
uetf = tf(z3, n3)
[G3Null G3Poll G3K ]  = tf2zp(z3, n3)
[G3r G3p G3k ]  = residue(z3, n3) 
TF2VN(z3,n3) 