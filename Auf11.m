clear
home
close all

te = 70;
dt = 0.1;
t_ = 0 : dt :te;
fmax = 1/(2*dt);
df = 1/te

s = 3 * ( sigma_(t_) - sigma_(t_ - 2) ); % Eingang signal 

%1 Fall - Mit quadrierung
S = dt * fft(s); %Spektrum Berechnung - FourierTransformierte
S_ = fftshift(S); % Shift des Spektrums
f_ = -fmax : df : fmax; %Vektor fuer das Frequnz
Skomp_ = S_ .* exp(-j*2*pi*f_*(dt/2)); %Kompensation fuer das Stuztelle von Matlab
Squadrat_ = Skomp_ .* Skomp_;  % Quadrieren des Spektrums
Sb = abs(Squadrat_); %Betrag des Spekturms
Sphase = angle(Squadrat_); %Phase des Spektrums


%Fall - Mit Faltung
y = dt * conv(s, s); %Faltung der Signal mit sich selbst
y = y ( 1:length(s) ); %Schneiden der Vektor
Y2 = dt*fft(y); %Spektrum der Faltung
Y2_ = fftshift(Y2); %
Y2komp_ = Y2_ .* exp(-j*2*pi*f_*(dt/2)); %Kompensation der Phasen Verschiebung
Y2b_ = abs(Y2komp_);
Y2phase_ = angle(Y2komp_);
                      
figure(1);
subplot(3,1,1);
plot(t_, s, 'linewidth', 2); 
grid;
title('Ursprungliche Funktion')

figure(1);
subplot(3,1,2);
plot(f_, Sb, 'linewidth', 2); 
grid;
title('abs (S (t) * S(t) )')

figure(1);
subplot(3,1,3);
plot(f_, Sphase, 'linewidth', 2); %Signal Funktion
grid;
title('phi (S (t) * S(t))')

figure(2);
subplot(3,1,1);
plot(t_, y, 'linewidth', 2); %Signal Funktion
grid;
title('Faltung s(t) * s(t)')

figure(2);
subplot(3,1,2);
plot( f_, Y2b_ , 'linewidth', 2); %Signal Funktion
grid;
title('Bertrag der Faltung')

figure(2);
subplot(3,1,3);
plot( f_, Y2phase_ , 'linewidth', 2); %Signal Funktion
grid;
title('Phase der Faltung')