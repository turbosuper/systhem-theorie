clear
home
close all

te = 70;
dt = 0.1;
t_ = 0 : dt :te;
fmax = 1/(2*dt);
df = 1/te

s = 3 * ( sigma_(t_) - sigma_(t_ - 2) ); % Eingang signal 

S = dt * fft(s);
 
S_ = fftshift(S);

f_ = -fmax : df : fmax;

Skomp_ = S_ .* exp(-j*2*pi*f_*(dt/2));

Sb = abs(Skomp_);

Sphase = angle(Skomp_);

                      
subplot(4,1,1);
plot(t_, s, 'linewidth', 2); %Signal Funktion
grid;
title('Ursprung')

subplot(4,1,2);
plot(f_, Sb, 'linewidth', 2); %Signal Funktion
grid;
title('Betrag')

subplot(4,1,3);
plot(f_, Sphase, 'linewidth', 2); %Signal Funktion
grid;
title('Phase')
