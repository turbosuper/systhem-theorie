 clear
home
close all

% Matrizen deklarieren
A = [ -5 1;
     1 0];

 b = [1 ; -1];
 
 c = [0 1];
 
 d = 0;
 
 % LTI Objekt erstellen
 zstm = ss( A, b, c, d) % 'ioDelay', Tt);
 
 % Graphische Darstellung
 uetf = tf(zstm)