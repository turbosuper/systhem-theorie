close all
clear
home

T = 0.1;
tk = 0: 0.01 : 20*T
td = 0: T : 20*T

s = tf('s');
z = tf('z', T);

Gk = 1/(s*(s + 1));

Gz = c2d(Gk, T, 'zoh')

sGk = step(Gk, tk);
sGz = step(Gz,td);

hold on
plot(sGk,tk)
stairs(sGz,td)
stem(sGz, td)
hold off