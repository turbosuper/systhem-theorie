clear
home
close all

t = 0:0.01:5;
s = 2*sin(2*pi*t - (pi/2));

plot(t,s)
grid on