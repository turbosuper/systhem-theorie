%-----------------------------------------------------------------------
% Musterlösung Übung Systemtheorie
% © Prof. Dr. Volker Sommer, Beuth Hochschule für Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all
disp('Übertragungsfunktion G(s) des Systems:')
z = [100];
n = [10 6 10.5 1];
G = tf(z, n)
disp('Zustandsform des Systems:')
sys = ss(G);
[A b c d] = ssdata(sys)
