%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.01;      % Zeitaufl�sung
te = 10;        % Endzeit (muss gr��er als Signall�nge sein!)
t = -te:dt:te;  % Zeitvektor

T = 2;
a = 5;
T0 = 4;
u = a*(sigma(t)-sigma(t-T0));   % u(t)
g = 1/T*sigma(t).*exp(-t/T);     % g(t)
gT0 = 1/T*sigma(T0-t).*exp(-(T0-t)/T);     % g(T0-t)

y = dt*conv(u, g); % Faltung berechnen
t1 = -2*te:dt:2*te;  % verdoppelter Zeitvektor zur Darstellung des Faltungsproduktes

subplot(3,1,1)
plot(t,u,'r','LineWidth',2)
title('u(t)')
grid

subplot(3,1,2)
plot(t,g,'b','LineWidth',2)
hold on
subplot(3,1,2)
plot(t,gT0,'b','LineWidth',1)
title('g(t), g(T0-t)')
grid

subplot(3,1,3)
plot(t1,y,'g','LineWidth',2)
title('y(t)=u(t)*g(t)')
grid minor      % grid mit feinerer Aufl�sung
