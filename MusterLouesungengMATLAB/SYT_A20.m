%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all

% Bodediagramm von G1(s) zeichnen 
% Berechnung des Frequenzganges durch die Funktion 'bode'
% Festlegung der Frequenzachse durch Auswertung der Eckfrequenzen
% Manuelles Zeichnen der Diagramme
z = [1 20 24];                          % Eingabe von Z�hler- und Nennerpolynom
n = [2 8 16 0];
G1 = tf(z, n)                           % Erzeugung eines LTI-Objektes von G1

% Algorithmus f�r die automatische Bestimmung der Frequenzachse des Bodediagramms
% Dazu zuerst Eliminierung evt. vorhandener Null- oder Polstellen bei s=0,
% dann Bestimmung der kleinsten und gr��ten Eckfrequenz w_min und w_max des Systems
% Frequenzachse beginnt zwei Zehnerpotenzen unterhalb von w_min
% und endet zwei Zehnerpotenzen oberhalb von w_max
i = length(z);
while z(i)==0 & i>1, i=i-1; end
z1 = z(1:i);                            % L�schen von Nullstellen der �bertragungsfunktion bei s=0
i = length(n);
while n(i)==0 & i>1, i=i-1; end
n1 = n(1:i);                            % L�schen von Polstellen der �bertragungsfunktion bei s=0
[null pol k] = tf2zp(z1, n1);           % Faktorisierung der �bertragungsfunktion
wz_min = 1e99;                          % Bestimmung der kleinsten und gr��ten Z�hlereckfrequenz
wz_max = 0;
if length(null) > 0
    wz_min = min(abs(null));              
    wz_max = max(abs(null));
end;
wn_min = 1e99;                          % Bestimmung der kleinsten und gr��ten Nennereckfrequenz
wn_max = 0;
if length(pol) > 0
    wn_min = min(abs(pol));
    wn_max = max(abs(pol));
end;
w_min = min(wz_min, wn_min);            % Ermittlung der kleinsten und gr��ten Eckfrequenz
w_max = max(wz_max, wn_max);
w = logspace(round(log10(w_min))-2, round(log10(w_max))+2, 500); % Festlegung der logarithmischen Frequenzachse

[betrag,phase] = bode(G1,w);	        % Berechnung des Bodediagramms aus LTI-Objekt
betrag=reshape(betrag,[1,length(w)]);   % Wandlung der von "bode" erzeugten ... 
phi=reshape(phase,[1,length(w)]);       % ... 3-D Matrizen in Zeilen-Vektoren
bdB=20*log10(betrag);				    % Umrechnung des Betrages in dB
figure('NumberTitle','off','Name', 'Bodediagramm von G1(s)')
subplot(2,1,1)
semilogx(w,bdB)						    % bdB linear �ber logarithmischer omega-Achse darstellen
grid on
title('Amplitudengang')
ylabel('dB')						
subplot(2,1,2)
semilogx(w,phi)					        % Phase linear �ber logarithmischer omega-Achse darstellen	
grid on
title('Phasengang')
xlabel('omega [1/sek]')
ylabel ('Grad')
pause

% Erg�nzung: Darstellung der Elementarsysteme von G1
tf2vn(z, n);                                % Zerlegung in Elementarsysteme
G1a = tf(1.5,1);                            % Verst�rkungsfaktor
s = tf('s');                                % LTI-Objekt f�r Elementarsystem 's' definieren 
G1b = 1/s;                                  % Integrator
G1c = (1 + 0.053425*s);                     % Vorhalteglied 1. Ordnung
G1d = (1 + 0.77991*s);                      % Vorhalteglied 1. Ordnung
G1e = 1/(1 + 2*0.70711*0.35355*s + 0.125*s^2);% Verz�gerungsglied 2. Ordnung
close(1)
figure('NumberTitle','off','Name', 'Zerlegung von G1 in Elementarsysteme (schwarz = Gesamtsystem)')
bode(G1a,'r',G1b,'b',G1c,'g',G1d,'y',G1e,'m', G1, 'k', w);
grid on
pause

% Bodedigramm von G2(s) zeichen
% Berechnung des Frequenzganges und
% Festlegung der Frequenzachse durch die Funktion 'bode
% Manuelles Zeichnen der Diagramme
z = [4 13 20];
n = conv([2 0 8],[1 1]);
G2 = tf(z, n)
tf2vn(z, n);
[betrag,phase,w] = bode(G2);	        % Berechnung des Bodediagramms einschlie�lich Frequenzachse aus LTI-Objekt
betrag=reshape(betrag,[1,length(w)]);   % Wandlung der von "bode" erzeugten ... 
phi=reshape(phase,[1,length(w)]);       % ... 3-D Matrizen in Zeilen-Vektoren
bdB=20*log10(betrag);				    % Umrechnung des Betrages in dB
figure('NumberTitle','off','Name', 'Bodediagramm von G2(s)')
subplot(2,1,1)
semilogx(w,bdB)						    % bdB linear �ber logarithmischer omega-Achse darstellen
grid on
title('Amplitudengang')
ylabel('dB')						
subplot(2,1,2)
semilogx(w,phi)					        % Phase linear �ber logarithmischer omega-Achse darstellen	
grid on
title('Phasengang')
xlabel('omega [1/sek]')
ylabel ('Grad')
pause

% Erg�nzung: Darstellung der Elementarsysteme von G2
tf2vn(z, n);                                    % Zerlegung in Elementarsysteme
G2a = tf(2.5,1);                                % Verst�rkungsfaktor
s = tf('s');                                    % LTI-Objekt f�r Elementarsystem 's' definieren 
G2b = (1 + 2*0.72672*0.44721*s + 0.2*s^2);      % Vorhalteglied 2. Ordnung
G2c = 1/(1 + 2*8.3267e-017*0.5*s + 0.25*s^2);   % Verz�gerungsglied 2. Ordnung (d=0)
G2d = 1/(1 + s);                                % Verz�gerungsglied 1. Ordnung
close(2)
figure('NumberTitle','off','Name', 'Zerlegung von G2 in Elementarsysteme (schwarz = Gesamtsystem)')
bode(G2a,'r',G2b,'b',G2c,'g',G2d,'y', G2, 'k', w);
grid on
pause

% Bodedigramm von G3(s) zeichen
% Berechnung des Frequenzganges und
% Festlegung der Frequenzachse durch die Funktion 'bode
% Zeichnen der Diagramme ebenfalls durch die Funktion 'bode'
z = [1];
n = [1 1 1];
G3 = tf(z, n, 'iodelay', 5)             % G3 mit Totzeitglied eingeben
tf2vn(z, n);
figure('NumberTitle','off','Name', 'Bodediagramm von G3(s)')
bode(G3)
grid on
