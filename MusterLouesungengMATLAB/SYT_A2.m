%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all
t = 0:0.05:5;
s = 2*sin(2*pi.*t-pi/2);
plot(t,s,'LineWidth',2)
grid on
title('s(t) = 2*sin(2*pi*t-pi/2)')