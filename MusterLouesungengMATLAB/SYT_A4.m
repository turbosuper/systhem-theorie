%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all
dt = 1e-3;      % Zeitaufl�sung
t = -5:dt:5;    % Zeitvektor definieren
T = 2;
% a)
s1 = sigma(t).*exp(-t/T);
subplot(3,2,1)
plot(t,s1,'r','LineWidth',2)
grid
title('a)')
E1 = dt * sum(s1.^2)
% b)
s2 = sigma(t/2-T);
subplot(3,2,2)
plot(t,s2,'r','LineWidth',2)
grid
title('b)')
% keine endliche Gesamtenergie
% c)
s3 = (t-T).*sigma(t-T);
subplot(3,2,3)
plot(t,s3,'r','LineWidth',2)
grid
title('c)')
% keine endliche Gesamtenergie
% d)
s4 = (sigma(t)-sigma(t-4)).*cos(2*pi/8*(t-2));
subplot(3,2,4)
plot(t,s4,'r','LineWidth',2)
grid
title('d)')
E4 = dt * sum(s4.^2)
% e)
s5 = sigma(T-t);
subplot(3,2,5)
plot(t,s5,'r','LineWidth',2)
grid
title('e)')
% keine endliche Gesamtenergie
% f)
s6 = 2*sigma(1-t.^2);
subplot(3,2,6)
plot(t,s6,'r','LineWidth',2)
grid
title('f)')
E6 = dt * sum(s6.^2)
