%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all
% G1(s)
disp('*******************   G1(s):  *********************')
z = [1 4];
n = [1 3];
G1 = tf(z, n)
figure('NumberTitle','off','Name', 'Sprungantwort von G1(s)')
step(G1)
grid
figure('NumberTitle','off','Name', 'Null-/Polstellenplan von G1(s)')
pzmap(G1)
% G2(s)
disp('*******************   G2(s):  *********************')
z = [1 5 4];
n = [1 4 8 0];
G2 = tf(z, n)
figure('NumberTitle','off','Name', 'Sprungantwort von G2(s)')
step(G2)
grid
figure('NumberTitle','off','Name', 'Null-/Polstellenplan von G2(s)')
pzmap(G2)
% G3(s)
disp('*******************   G3(s):  *********************')
z = [2 3];
n = [1 2 -3];
G3 = tf(z, n)
figure('NumberTitle','off','Name', 'Sprungantwort von G3(s)')
step(G3)
grid
figure('NumberTitle','off','Name', 'Null-/Polstellenplan von G3(s)')
pzmap(G3)


