%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.01;      % Zeitaufl�sung
te = 10;        % Endzeit (muss gr��er als Signall�nge sein!)
t = -te:dt:te;    % Zeitvektor (symmetrisch um Nullpunkt)

s1 = sigma(t+10)-2*sigma(t+7)+2*sigma(t+6)-2*sigma(t+5)+2*sigma(t+3)-2*sigma(t+1)...
    +2*sigma(t)-2*sigma(t-1)+2*sigma(t-3)-2*sigma(t-4)+2*sigma(t-5)-2*sigma(t-7)+2*sigma(t-9)-sigma(t-10);% Signal 1

s1_ = fliplr(s1);       % s1 spiegeln

phiEs1s2 = dt*conv(s1_,s1); % Faltung berechnen
% Durch conv() entsteht ein Vektor mit doppelter Anzahl von Elementen,
% deshalb f�r Darstellung neuen Zeitvektor definieren:
t1 = -2*te:dt:2*te;         

subplot(2,1,1)
plot(t,s1,'r','LineWidth',2)
title('s1(t)')
grid
subplot(2,1,2)
plot(t1,phiEs1s2,'g','LineWidth',2)
title('Korrelation(s1,s2)')
grid minor      % grid mit feinerer Aufl�sung
