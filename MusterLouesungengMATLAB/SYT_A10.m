%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.2;     % Zeitinkrement
te = 200;      % Endzeit (muss gr��er als Signall�nge sein!)
t = 0:dt:te;   % Zeitvektor

a = 3;
T0 = 2;
u = a*(sigma(t+T0)-sigma(t-T0));   % u(t)

df = 1/te;      % Frequenzaufl�sung
fmax = 1/dt/2;
f =-fmax:df:fmax; % Frequenzvektor
U = dt*fftshift(fft(u)); % Fast Fourier-Transformation berechnen
U = U.*exp(-j*2*pi*f*dt/2); % Kompensation der Phasenverschiebung dt/2 zwischen u(t) und u
subplot(3,1,1)
plot(t,u,'LineWidth',2)
title('u(t)')
grid
subplot(3,1,2)
plot(f,abs(U),'r','LineWidth',2)
title('abs(U(f))')
grid minor
subplot(3,1,3)
plot(f,angle(U),'r','LineWidth',2)
title('phi(U(f))')
grid minor
