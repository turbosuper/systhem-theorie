%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

% Diese Funktion berechnet f�r einen �bergebenen Vektor t die Funktion s=sigma(t)
function u = sigma_(x);
u = zeros(1, length(x)); % Erzeugung eines Nullvektors der L�nge von t
for i = 1:length(x)
  if x(i) >= 0
      u(i) = 1;
  end
end    
% alternative L�sung ohne Schleife:
% u = (x >= zeros(size(x)));
% u = x >= 0