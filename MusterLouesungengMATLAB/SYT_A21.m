%-----------------------------------------------------------------------
% Musterlösung Übung Systemtheorie
% © Prof. Dr. Volker Sommer, Beuth Hochschule für Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all

% Übertragungsfunktion muss manuell aus dem
% gegebenem Amplitudengang bestimmt werden. Es folgt: 
z = [10 0];
n = [1 70];
G = tf(z, n)   % LTI-Objekt eingeben
% Frequenzgang des Systems zeichnen:
w = logspace(-1,4, 200);
figure('NumberTitle','off','Name', 'Bodediagramm von G(s)')
bode(G,w)
grid on
