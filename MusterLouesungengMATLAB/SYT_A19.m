%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all

w = logspace(-3, 3, 200);        % Frequenzachse abh�ngig von den Eckfrequenzen vorgeben
G_omega =  10*(1+j*w)./(j*w)./(1+10*j*w); % Eingabe des Frequenzganges als komplexe Funktion
bdB = 20*log10(abs(G_omega));    % Betrag bilden und in Dezibel umwandeln
phi = 180/pi*angle(G_omega);     % Phase bilden
figure(1)						 % Grafikbildschirm �ffnen
subplot(2,1,1)
semilogx(w,bdB)				     % bdB linear �ber logarithmischer omega-Achse darstellen
grid on
title('Amplitudengang')
ylabel('dB')						
subplot(2,1,2)
semilogx(w,phi)					 % Phase linear �ber logarithmischer omega-Achse darstellen	
grid on
title('Phasengang')
xlabel('omega [1/sek]')
ylabel ('Grad')

% alternative L�sung mit der Matlab-Funktion 'bode'
%w = logspace(-3, 3, 200);
%G = tf([10 10],[10 1 0]);
%figure(2)
%bode(G, w)

