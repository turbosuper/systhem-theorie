%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all
A=[-5,1;1,0];   % Systemmatrix
b=[1;-1];       % Eingangsvektor
c=[0,1];        % Ausgangsvektor
d=0;            % Durchgangsfaktor
disp('Zustandsform des Systems:')
ss_sys = ss(A,b,c,d)   % LTI-System aus Zustandsform in Matlab erzeugen
disp('�bertragungsfunktion G(s) des Systems:')
tf_sys = tf(ss_sys)     % Umwandeln in �bertragungsfunktion
[z1 n1] = tfdata(tf_sys);
disp('Z�hler- und Nennerpolynom von G(s):')
z = z1{1,1}
n = n1{1,1}
