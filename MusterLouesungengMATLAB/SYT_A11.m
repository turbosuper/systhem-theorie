%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.2;      % Zeitinkrement
te = 200;      % Endzeit (muss gr��er als Signall�nge sein!)
t = 0:dt:te;   % Zeitvektor

a = 3;
T0 = 2;
u = a*(sigma(t)-sigma(t-T0));   % u(t)

% 1. L�sungsvariante: Quadrierung des Spektrums von u(t) aus A10
df = 1/te;      % Frequenzaufl�sung
fmax = 1/dt/2;
f = -fmax:df:fmax; % Frequenzvektor
U = fftshift(fft(u)*dt); % Fast Fourier-Transformation berechnen
% Kompensation der Phasenverschiebung dt/2 zwischen u(t) und u
U = U.*exp(-j*2*pi*f*dt/2);
Y = U.*U;
figure('NumberTitle','off','Name', '1. Variante: Quadrierung des Spektrums')
subplot(3,1,1)
plot(t,u,'LineWidth',2)
title('u(t)')
grid
subplot(3,1,2)
plot(f,abs(Y),'LineWidth',2)
title('abs(U(f)*U(f))')
grid minor
subplot(3,1,3)
plot(f,angle(Y),'LineWidth',2)
title('phi(U(f)*U(f))')
grid minor

% 2. L�sungsvariante: Faltung von u(t) mit sich selbst und danach
% Berechnung des Spektrums des Faltungsproduktes
y2 = dt*conv(u,u);
y2 = y2(1:length(u)); % Beschr�nkung von y2 auf die L�nge von u
Y2 = fftshift(fft(y2)*dt); % Fast Fourier-Transformation berechnen
% Kompensation der Phasenverschiebung dt zwischen y(t) und y2
Y2 = Y2.*exp(-j*2*pi*f*dt); 
figure('NumberTitle','off','Name', '2. Variante: Faltung von u(t)')
subplot(3,1,1)
plot(t,y2,'LineWidth',2)
title('y2(t)')
grid
subplot(3,1,2)
plot(f,abs(Y2),'LineWidth',2)
title('abs(Y2(f))')
grid minor
subplot(3,1,3)
plot(f,angle(Y2),'LineWidth',2)
title('phi(Y2(f))')
grid minor

% 3. L�sungsvariante: Eingabe von y(t) mit sigma-Funktion 
y3 = a^2*(t.*sigma(t) - 2*(t-T0).*sigma(t-T0) + (t-2*T0).*sigma(t-2*T0));
Y3 = fftshift(fft(y3)*dt); % Fast Fourier-Transformation berechnen
% Keine Phasenkompensation zwischen y(t) und y3 erforderlich!
figure('NumberTitle','off','Name', '3. Variante: Eingabe von y(t)')
subplot(3,1,1)
plot(t,y3,'LineWidth',2)
title('y3(t)')
grid
subplot(3,1,2)
plot(f,abs(Y3),'LineWidth',2)
title('abs(Y3(f))')
grid minor
subplot(3,1,3)
plot(f,angle(Y3),'LineWidth',2)
title('phi(Y3(f))')
grid minor