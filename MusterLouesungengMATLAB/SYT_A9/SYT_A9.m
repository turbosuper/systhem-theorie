%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

% verwendet die Simulink-Datei "GST_A9_mod" und "GST_A9_err"

clear
home
close all

SYT_A9_mod	% �ffnen der Simulink-Strunktur des nichtlinearen Modells

disp('�berpr�fung der Reihenfolge der Zust�nde:')
[not_used,not_used,z]=SYT_A9_mod;
Zustandsreihenfolge=z

disp('Bestimmung des Arbeitspunktes:')
disp(' ')
uA = input('Bitte geben Sie die Eingangsgr��e uA ein: ')
xA = input('Bitte geben Sie den Vektor der Zustandsgr��en xA ein: ')
%xA=[2;1.414]	% AP des Systems
%uA=2

disp('Linearisiertes Modell:')
[A,b,c,d]=linmod('SYT_A9_mod',xA,uA)	% Linearisierung
sys = ss(A,b,c,d);
G = tf(sys)
pause

figure(1)
bode(A,b,c,d)
grid
title('Bodediagramm des von Matlab linearisierten Systems')
pause

SYT_A9_err	% �ffnen der Simulink-Struktur zum Vergleich
            %   zwischem linearen und nichtlinearem Modell
