%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home
clear
close all
% G1(s)
z1 = [1 2];
n1 = [1 4 3 0];
disp('*******************   G1(s):  *********************')
G1 = tf(z1, n1)
disp('Produktform von G1(s):')
[null pol k] = tf2zp(z1, n1)
disp('Partialbruchform von G1(s):')
[R P K] = residue(z1, n1)
disp('V-Normalform von G1(s):')
tf2vn(z1, n1)
pause
% G2(s)
z2 = [4 20 24];
n2 = [2 8 16 0];
disp('*******************   G2(s):  *********************')
G2 = tf(z2, n2)
disp('Produktform von G2(s):')
[null pol k] = tf2zp(z2, n2)
disp('Partialbruchform von G2(s):')
[R P K] = residue(z2, n2)
disp('V-Normalform von G2(s):')
tf2vn(z2, n2)
pause
% G3(s)
z3 = [1];
n3 = [1 1 0 0];
disp('*******************   G3(s):  *********************')
G3 = tf(z2, n2)
disp('Produktform von G3(s):')
[null pol k] = tf2zp(z3, n3)
disp('Partialbruchform von G3(s):')
[R P K] = residue(z3, n3)
disp('V-Normalform von G3(s):')
tf2vn(z3, n3)
