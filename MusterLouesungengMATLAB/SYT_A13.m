%-----------------------------------------------------------------------
% Musterlösung Übung Systemtheorie
% © Prof. Dr. Volker Sommer, Beuth Hochschule für Technik Berlin
%-----------------------------------------------------------------------

clear
home
close all

U0 = 12;  % 
T = 0.05; % Zeitkonstante des Systems in Sekunden mit T = L/R
R = 1e3;  % Widerstandswert in Ohm

G = tf([1], [T 1]);             % Übertragungsfunktion als LTI-Objekt eingeben
t = linspace(0, 4*T, 500);      % Zeitvektor festlegen
u1 = U0/R*sigma(t);             % Eingangssprung der Amplitude U0
y1 = lsim(G, u1, t);            % Sprungantwort des Systems
u2 = U0*sin(t*2*pi/T);           % eingeschaltetes Sinussignal
y2 = lsim(G, u2, t);            % Systemantwort auf u2(t)
subplot(2,1,1)
plot(t,u1,t,y1,'LineWidth', 2);
subplot(2,1,2)
plot(t,u2,t,y2,'LineWidth', 2);
