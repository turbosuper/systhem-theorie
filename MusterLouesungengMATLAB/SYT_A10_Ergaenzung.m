%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.1;     % Zeitinkrement
te = 100;      % Endzeit (muss gr��er als Signall�nge sein!)
t = 0:dt:te;   % Zeitvektor

s = 0.5*sin(pi*t).*(sigma_(t)-sigma_(t-2));   % s(t)

df = 1/te;      % Frequenzaufl�sung
fmax = 1/(2*dt);
om =2*pi*(-fmax:df:fmax); % Frequenzvektor
S = dt*fftshift(fft(s)); % Fast Fourier-Transformation berechnen
subplot(2,1,1)
plot(t,s,'LineWidth',2)
title('s(t)')
grid
subplot(2,1,2)
plot(om,abs(S),'r','LineWidth',2)
title('abs(S(omega))')
grid minor
