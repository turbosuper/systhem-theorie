%-----------------------------------------------------------------------
% Musterl�sung �bung Systemtheorie
% � Prof. Dr. Volker Sommer, Beuth Hochschule f�r Technik Berlin
%-----------------------------------------------------------------------

home 
clear
close all

dt = 0.01;      % Zeitaufl�sung
te = 10;        % Endzeit (muss gr��er als Signall�nge sein!)
t = -te:dt:te;  % Zeitvektor

T = 2;
a = 5;
T0 = 4;
u = a*(sigma(t)-sigma(t-T0));   % u(t)
u1 = a*(sigma(t-2)-sigma(t-2-T0));   % u(t-2)

y = dt*conv(u, u1); % Faltung berechnen
t1 = -2*te:dt:2*te;  % verdoppelter Zeitvektor zur Darstellung des Faltungsproduktes

subplot(3,1,1)
plot(t,u,'r','LineWidth',2)
hold on
title('u(t)')
grid

subplot(3,1,2)
plot(t,u1,'b','LineWidth',2)
title('u(t-2)')
grid

subplot(3,1,3)
plot(t1,y,'g','LineWidth',2)
title('y(t)=u(t)*u(t-2)')
grid minor      % grid mit feinerer Aufl�sung
