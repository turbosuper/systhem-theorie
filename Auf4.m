clear
home
close all

ta = -10;
te = 10;
dt = 0.001;
t_ = ta : dt : te; %Zeit Vektor bestimmen
T = 2; % obere Grenze geben

subplot(3,2,1)
s1_ = sigma_(t_) .* exp(-t_/T);
plot(t_,s1_)

%Integral bauen aus summe der Rechtecks
s1_q = s1_ .^2; 
e1_ =  sum(s1_q) * dt 

subplot(3,2,2);
s2_ = sigma_( (1/2) * t_ - T);
plot(t_,s2_)
e2_ = 'unendlich'

subplot(3,2,3);
s3_ = (t_ - T) .* sigma_(t_ - T);
plot(t_, s3_)
e3_ = 'unendlich'

subplot(3, 2, 4);
s4_ = (sigma_(t_) - sigma_(t_ - 4)) .* cos(( 2*pi()/8 )* (t_ - 2));
plot(t_, s4_)

sin_q = s4_ .^2;
e4_ = sum(sin_q) * dt

subplot(3, 2, 5);
s5_ = sigma_(T - t_);
plot(t_, s5_)
e5_ = 'unendlich'

subplot(3, 2, 6);
s6_ = 2 * sigma_(1 - t_ .^2);
plot(t_, s6_)
q6 = s6_ .^2;
e6 = sum(q6)*dt
