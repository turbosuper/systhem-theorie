close all
clear
home

T = 0.01;

%Zustandsmodel erzeugen
[A, b, c , d] = dlinmod('Auf27sim');

%Ubertragungsfunktion aus zustandsmodel Erzuegen
sys_d = ss(A, b, c, d, T);

uetf = tf(sys_d)