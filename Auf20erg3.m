clear
home
close all

%Logaritmischgetelte  Achse erzeugen
w = logspace (-1, 3, 500);

%G2  System mithilfer Dummy Funktion darstellen
s = tf('s');
uetf3 = (exp(-5*s))/(s^2+s+1);

% Zaehler und Nenner der G2 ermitteln
[z2, n2] = tfdata(uetf3) ;

%...da z und n kommen aus Arrays, wir nehmen benutzten der erster Element
z2 = z2{1,1};
n2 = n2{1,1};

%in V-Normalform darstellen
TF2VN(z2,n2);
s = tf('s');

G2_a = tf(2.5,1); %Ubertragensfunktion von V fuer bode()
G2_b =  1/(1 + 2*0.5*1*s + 1*s^2) %Vorhaltglied 2 Ordnung

subplot(2,2,1)
bode(G2_a, w)
title('Darstellung der Verstaerkung')
grid on

subplot(2,2,3)
bode(G2_b, w)
title('Vorhalteglied 2. Ordnung')
grid on


subplot(2,2,2)
bode(G2_a, 'b', G2_b, 'r', w)
title('Darstellung der Ganzen summe')
grid on

subplot(2,2,4)
bode(uetf3)
title('Darstellung der LTI')
grid on