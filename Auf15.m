clear
home
close all

% Zaehler und Nenner deklarieren
z = [0 0 0 100];  
n = [10 6 10.5 1];
 
 % LTI Objekt fuer Ubertragungsfunktion Darstellen
 uetf = tf(z, n) 
 
 % Graphische Darstellung
 zstm = ss(uetf)
 
 %In Richtige Vektoren darstellen
 [A b c d] = ssdata(zstm)
 