close all
clear
home

T = 1;

td = 0:1:5

z = [0.5];
n = [1 -1];

Gz = tf(z, n, T)

uk = sigma_(td)

[y, ts, x] = lsim(Gz,uk,td)
stem(y, td)