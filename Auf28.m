close all
clear
home

T = 0.1;
td = 0: T: 5

z = tf('z',T);

Gz = 0.7/(z^2 - 0.8*z + 0.15);


subplot(2,1,1)
pzmap(Gz)
title('Poll- / Nullstellen plan fuer G(z)')
subplot(2,1,2)
%plot(h, td)
step(Gz,td)
title('Sprungantwort fesstellen')