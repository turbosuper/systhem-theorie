clear
home
close all

%  G1 Zaehler und Nenner deklarieren
z1 = [0 1 20 24];  
n1 = [2 8 16 0];

%G2  System mithilfer Dummy Funktion darstellen
s = tf('s');
uetf2 = (4*s^2+13*s+20)/((2*s^2+8)*(s+1));

%G3  System mithilfer Dummy Funktion darstellen
uetf3 = (exp(-5*s))/(s^2+s+1);

% Ubertragunsfunktion der Polynom 
uetf1 = tf(z1, n1);

%Logaritmischgetelte  Achse erzeugen
w = logspace (-1, 3, 500);

% Betrag und Phase berechnen
[betrag1, phase1] = bode(uetf1,w);

% Betrag und Phase ins Vekktoen umwandlend
betrag1 = reshape(betrag1, [1,length(w)]);
phase1 = reshape(phase1, [1, length(w)]);

%Betraf und Phase im dB umrechnenen
G1abs = 20*log10(betrag1); %Betrag in dB

%G2 Frequenzachse von Bode funktion bestimmen
[betrag2, phase2, w2] = bode(uetf2);
betrag2 = reshape(betrag2, [1,length(w2)]);
phase2 = reshape(phase2, [1, length(w2)]);
G2abs = 20*log10(betrag2); %Betrag in dB


%Die Grahpen zeichen
subplot(3,3,1)
semilogx(w, G1abs)
title('Betrag der G1(w)')


subplot(3,3,2)
semilogx(w, phase1)
title('Phase der G1(w)')

subplot(3,3,3)
bode(uetf1);
title('G1 mit bode() fuer testen')

subplot(3,3,4)
semilogx(w2, G2abs)
title('Betrag der G2(w)')

subplot(3,3,5)
semilogx(w2, phase2)
title('Phase der G2(w)')

subplot(3,3,6)
bode(uetf2);
title('G2 mit bode() fuer testen')

subplot(3,3,7);
bode(uetf3);
title('G3 mit bode()')
