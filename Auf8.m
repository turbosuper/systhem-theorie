clear
home
close all

R = 1E3     %1 kOhm
C = 1E-3    %1 mF
Uc = 5      %5 V
te = 10;
dt = 0.001;
t_ = -te: dt :te;
T = 2; %Zeitkonstate
a = 5; %Konstatne fuer Sigma Funktion
T0 = 4; %Dauer der Eingangs Impuls

u = a * ( sigma_(t_) - sigma_(t_ - T0) ); % Eingang signal 
u1 = a * ( sigma_(t_ - T) - sigma_(t_ - T0 - T) );% Verschobene Eingang Impuls%
%g0 = (1/T) .* sigma_(t_) .* exp(-t_ / T); % Sistem Funktion g(t)

t1_ = -2*te : dt : 2*te;  %die conv Funktion erzeugt doppelte lange,
                          %dazu diese Umformung
                            
y = dt * conv(u, u1); %Ausgang: 
                      %Ubertragungs Funktion mit Hilfe der Faltungsintegral
%g1 = (1/T) .* sigma_(T - t_ ) .* exp(-(T- t_) / T );
%y1 = dt * conv(u1, g1);
                      
subplot(3,1,1);
plot(t_, u, 'linewidth', 2); %Signal Funktion
title('Urschprunglicher Eingang Signal')

subplot(3,1,2);
plot(t_, u1, 'linewidth', 2); 
title('Verschobene Eingang Signal')

subplot(3,1,3);
plot(t1_, y, 'linewidth', 2); %Ubertragungs Funktion
title('Antwort zu dem verschobene Signal')